<?php

/**
 * @file
 * Functions file for projectus.
 */

/**
 * GET DEFAULT VALUES FUNCTIONS.
 */

/**
 * Define status values for a content type.
 */
function projectus_get_status() {

  $projects_status = array(
    'initiated' => t('initiated'),
    'in_progress' => t('in progress'),
    'finished' => t('finished'),
  );

  return $projects_status;
}

/**
 * Define priorities values for a content type.
 */
function projectus_get_priorities() {

  $projects_priorities = array();

  $projects_priorities[1] = 'low';
  $projects_priorities[2] = 'medium';
  $projects_priorities[3] = 'urgent';

  return $projects_priorities;
}

/**
 * Define categories values for a content type.
 */
function projectus_get_categories() {

  $projects_categories = array();

  $projects_categories[1] = 'development';
  $projects_categories[2] = 'new funcionality';
  $projects_categories[3] = 'incidence';

  return $projects_categories;
}

/**
 * Define timetrackings types.
 */
function projectus_get_types_timetrackings() {

  $types = array();

  $types['normal'] = 'normal';
  $types['extra'] = 'extra';

  return $types;
}

/**
 * GET VALUES FROM DATABASE FUNCTIONS.
 */

/**
 * Gets all clients from database.
 *
 * Returns array[nid]=title.
 */
function projectus_get_all_clients() {

  $clients_query = db_select('node', 'n');
  $clients_query->join('projectus_clients', 'pus_cli', 'n.nid = pus_cli.nid');
  $clients_query
  ->fields('n', array('nid', 'title'))
  ->condition('n.status', 1)
  ->condition('n.type', 'projectus_clients')
  ->condition('pus_cli.active', 1)
  ->orderBy('n.title', 'ASC');

  $clients_result = $clients_query->execute();
  $clients = array();
  foreach ($clients_result as $client) {
    $clients[$client->nid] = $client->title;
  }

  return $clients;
}

/**
 * Gets all projects from database.
 *
 * Returns array[nid]=title.
 */
function projectus_get_all_projects() {

  $projects_query = db_select('node', 'n');
  $projects_query->join('projectus_projects', 'pus_proj', 'n.nid = pus_proj.nid');
  $projects_query
  ->fields('n', array('nid', 'title'))
  ->condition('n.status', 1)
  ->condition('n.type', 'projectus_projects')
  ->condition('pus_proj.active', 1)
  ->orderBy('n.title', 'ASC');

  $projects_result = $projects_query->execute();
  $projects = array();
  foreach ($projects_result as $project) {
    $projects[$project->nid] = $project->title;
  }

  return $projects;
}

/**
 * Gets all tasks from database.
 *
 * Returns array[nid]=title.
 */
function projectus_get_all_tasks() {

  $tasks_query = db_select('node', 'n');
  $tasks_query->join('projectus_tasks', 'pt', 'n.nid = pt.nid');
  $tasks_query
  ->fields('n', array('nid', 'title'))
  ->condition('n.status', 1)
  ->condition('n.type', 'projectus_tasks')
  ->condition('pt.active', 1)
  ->orderBy('n.title', 'ASC');

  $tasks_result = $tasks_query->execute();
  $tasks = array();
  foreach ($tasks_result as $task) {
    $tasks[$task->nid] = $task->title;
  }

  return $tasks;
}

/**
 * Gets all users from database.
 *
 * Returns array[uid]=name.
 */
function projectus_get_all_users() {

  $users_query = db_select('users', 'u');
  $users_query
  ->fields('u', array('uid', 'name'))
  ->condition('u.uid', 0, '<>')
  ->orderBy('u.name', 'ASC');

  $users_result = $users_query->execute();
  $array_users = array();
  foreach ($users_result as $user) {
    $array_users[$user->uid] = $user->name;
  }

  return $array_users;
}

/**
 * Gets users by project from intermediate table 'projectus_projects_users'.
 *
 * Returns array[uid]=uid.
 */
function projectus_get_users_by_project($id_project) {

  $users_query = db_select('projectus_projects_users', 'pus_pu');
  $users_query
  ->fields('pus_pu', array('id_user'))
  ->condition('pus_pu.id_project', $id_project, '=');

  $users_result = $users_query->execute();
  $array_users = array();
  foreach ($users_result as $user) {
    $array_users[$user->id_user] = $user->id_user;
  }

  return $array_users;
}

/**
 * Gets users by project from intermediate table 'projectus_projects_users'.
 *
 * Returns array[uid]=name.
 */
function projectus_get_users_name_by_project($id_project) {

  $users_query = db_select('projectus_projects_users', 'ppu');
  $users_query->join('users', 'u', 'u.uid = ppu.id_user');
  $users_query
  ->fields('u', array('uid', 'name'))
  ->condition('ppu.id_project', $id_project, '=');

  $users_result = $users_query->execute();
  $array_users = array();
  foreach ($users_result as $user) {
    $array_users[$user->uid] = $user->name;
  }

  return $array_users;
}

/**
 * Gets users by task from intermediate table 'projectus_tasks_users'.
 *
 * Returns array[uid]=uid.
 */
function projectus_get_users_by_task($id_task) {

  $users_query = db_select('projectus_tasks_users', 'pus_tu');
  $users_query
  ->fields('pus_tu', array('id_user'))
  ->condition('pus_tu.id_task', $id_task, '=');

  $users_result = $users_query->execute();
  $array_users = array();
  foreach ($users_result as $user) {
    $array_users[$user->id_user] = $user->id_user;
  }

  return $array_users;
}

/**
 * Gets all the user tasks (assigned by project and task) from database.
 *
 * Returns array[nid]=name.
 */
function projectus_get_tasks_by_user($uid) {

  $tasks_by_user = array();

  // Tasks by user actives.
  $query = db_select('projectus_tasks_users', 'ptu');
  $query->join('projectus_tasks', 'pt', 'pt.nid = ptu.id_task');
  $query
  ->fields('pt', array('nid', 'name'))
  ->condition('ptu.id_user', $uid)
  ->condition('pt.active', 1)
  ->orderBy('pt.name', 'ASC');

  $result = $query->execute();
  foreach ($result as $task) {
    $tasks_by_user[$task->nid] = $task->name;
  }

  return $tasks_by_user;
}

/**
 * Gets the user tasks (assigned by project and task) selected from database.
 *
 * Returns array[nid]=name.
 */
function projectus_get_tasks_by_user_selected($uid) {

  $tasks_by_user = array();

  // Tasks by user actives.
  $query = db_select('projectus_tasks_users', 'ptu');
  $query->join('projectus_tasks', 'pt', 'pt.nid = ptu.id_task');
  $query
  ->fields('pt', array('nid', 'name'))
  ->condition('ptu.id_user', $uid)
  ->condition('pt.active', 1)
  ->condition('ptu.selected', 1)
  ->orderBy('pt.name', 'ASC');

  $result = $query->execute();
  foreach ($result as $task) {
    $tasks_by_user[$task->nid] = $task->name;
  }

  return $tasks_by_user;
}

/**
 * Get timetracking hours by task, user and date.
 *
 * Return hours.
 */
function projectus_get_hours_by_task_day_user($nid, $day, $uid) {

  $query = db_select('node', 'n');
  $query->join('projectus_timetrackings', 'pt', 'n.nid = pt.nid');
  $query
  ->fields('pt', array('hours'))
  ->condition('pt.id_task', $nid)
  ->condition('date_timetracking', $day)
  ->condition('n.uid', $uid)
  ->condition('n.status', 1);

  $hours = $query->execute()->fetchField();

  if (empty($hours)) {
    $hours = 0;
  }

  return $hours;
}

/**
 * Get if timetracking is validate by task, user and date.
 *
 * Return bool.
 */
function projectus_get_validate_by_task_day_user($nid, $day, $uid) {

  $query = db_select('node', 'n');
  $query->join('projectus_timetrackings', 'pt', 'n.nid = pt.nid');
  $query
  ->fields('pt', array('validate'))
  ->condition('pt.id_task', $nid)
  ->condition('date_timetracking', $day)
  ->condition('n.uid', $uid)
  ->condition('n.status', 1);

  $result = $query->execute();

  if ($result->rowCount() == 0) {
    $validate = -1;
  }
  else {
    $validate = $result->fetchField();
  }

  return $validate;
}

/**
 * Get selected field by task and user.
 *
 * Return 0/1.
 */
function projectus_timetrackings_get_selected_task_by_user($nid, $uid) {

  $query = db_select('projectus_tasks_users', 'ptu');
  $query
  ->fields('ptu', array('selected'))
  ->condition('ptu.id_task', $nid)
  ->condition('ptu.id_user', $uid);

  $selected = $query->execute()->fetchField();

  return $selected;
}

/**
 * Get tasks by project.
 *
 * Return tasks.
 */
function projectus_projects_get_tasks_by_project($id_project) {

  $tasks = array();

  $query = db_select('projectus_tasks_users', 'ptu');
  $query->join('projectus_tasks', 'pt', 'pt.nid = ptu.id_task');
  $query
  ->fields('ptu', array('id_task'))
  ->condition('ptu.id_project', $id_project)
  ->condition('pt.active', 1);

  $result = $query->execute();
  foreach ($result as $task) {
    $tasks[$task->id_task] = $task->id_task;
  }

  return $tasks;
}

/**
 * Get projects by client.
 *
 * Return projects.
 */
function projectus_projects_get_projects_by_client($id_client) {

  $projects = array();

  $query = db_select('projectus_projects', 'pt');
  $query
  ->fields('pt', array('nid'))
  ->condition('pt.id_client', $id_client)
  ->condition('pt.active', 1);

  $result = $query->execute();
  foreach ($result as $project) {
    $projects[$project->nid] = $project->nid;
  }

  return $projects;
}

/**
 * Get incurred hours.
 *
 * Returns array[nid]=hours.
 */
function projectus_reports_get_hours($uid, $validate, $client, $project, $task, $start, $end) {

  $hours = array();

  $query = db_select('node', 'n');
  $query->join('projectus_timetrackings', 'pt', 'n.nid = pt.nid');
  $query->join('projectus_tasks', 'pta', 'pt.id_task = pta.nid');
  $query->join('projectus_projects', 'pp', 'pta.id_project = pp.nid');
  $query->join('projectus_clients', 'pc', 'pp.id_client = pc.nid');
  $query
  ->fields('pt', array('nid'))
  ->condition('n.uid', $uid)
  ->condition('pt.validate', $validate)
  ->condition('pt.date_timetracking', $start, '>=')
  ->condition('pt.date_timetracking', $end, '<=');
  $query->addExpression('SUM(hours)', 'sum_hours');

  if (!empty($client)) {
    $query->condition('pc.nid', $client);
  }
  if (!empty($project)) {
    $query->condition('pp.nid', $project);
  }
  if (!empty($task)) {
    $query->condition('pta.nid', $task);
  }

  $result = $query->execute();
  foreach ($result as $user) {
    $hours = $user->sum_hours;
  }

  return $hours;
}

/**
 * Get name projects by client.
 *
 * Return name projects.
 */
function projectus_reports_get_name_projects_by_client($id_client) {

  $projects_query = db_select('node', 'n');
  $projects_query->join('projectus_projects', 'pus_proj', 'n.nid = pus_proj.nid');
  $projects_query
  ->fields('n', array('nid', 'title'))
  ->condition('n.status', 1)
  ->condition('n.type', 'projectus_projects')
  ->condition('pus_proj.active', 1)
  ->condition('pus_proj.id_client', $id_client)
  ->orderBy('n.title', 'ASC');

  $projects_result = $projects_query->execute();
  $projects = array();
  foreach ($projects_result as $project) {
    $projects[$project->nid] = $project->title;
  }

  return $projects;
}

/**
 * Get name tasks by project.
 *
 * Return name tasks.
 */
function projectus_reports_get_name_tasks_by_project($id_project) {

  $tasks = array();

  $query = db_select('projectus_tasks_users', 'ptu');
  $query->join('projectus_tasks', 'pt', 'pt.nid = ptu.id_task');
  $query->join('node', 'n', 'n.nid = pt.nid');
  $query
  ->fields('ptu', array('id_task'))
  ->fields('n', array('title'))
  ->condition('ptu.id_project', $id_project)
  ->condition('pt.active', 1);

  $result = $query->execute();
  foreach ($result as $task) {
    $tasks[$task->id_task] = $task->title;
  }

  return $tasks;
}


/**
 * CHECK FUNCTIONS.
 */

/**
 * Check if exist clients actives in database.
 *
 * Return bool.
 */
function projectus_exist_clients_actives() {

  $exist_clients_actives = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_clients pc, node n
                   WHERE pc.active = 1
                   AND pc.nid = n.nid)"
  )->fetchField();
  return $exist_clients_actives;
}

/**
 * Check if exist projects actives in database.
 *
 * Return bool.
 */
function projectus_exist_projects_actives() {

  $exist_projects_actives = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_projects pp, node n
                   WHERE pp.active = 1
                   AND pp.nid = n.nid)"
  )->fetchField();
  return $exist_projects_actives;
}

/**
 * Check if exist tasks actives in database.
 *
 * Return bool.
 */
function projectus_exist_tasks_actives() {

  $exist_tasks_actives = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_tasks pt, node n
                   WHERE pt.active = 1
                   AND pt.nid = n.nid)"
  )->fetchField();
  return $exist_tasks_actives;
}

/**
 * Check if exist projects actives for a especified client in database.
 *
 * Return bool.
 */
function projectus_exist_projects_by_client($id_client) {

  $exist_projects = db_query("select exists
                  (SELECT id_client
                   FROM projectus_projects
                   WHERE id_client = " . $id_client . "
                   AND active = 1)"
  )->fetchField();

  return $exist_projects;
}

/**
 * Check if exist tasks finished actives for a especified project in database.
 *
 * Return bool.
 */
function projectus_exist_tasks_not_finished_by_project($id_project) {

  $exist_tasks = db_query("select exists
                  (SELECT id_project
                   FROM projectus_tasks
                   WHERE id_project = " . $id_project . "
                   AND active = 1
                   AND status_task <> 'finished')"
  )->fetchField();
  return $exist_tasks;
}

/**
 * Check if exist projects finished actives for a especified client in database.
 *
 * Return bool.
 */
function projectus_exist_projects_not_finished_by_client($id_client) {

  $exist_tasks = db_query("select exists
                  (SELECT id_client
                   FROM projectus_projects
                   WHERE id_client = " . $id_client . "
                   AND active = 1
                   AND status_project <> 'finished')"
  )->fetchField();
  return $exist_tasks;
}

/**
 * Check if exist timetrackings actives for a especified task in database.
 *
 * Return bool.
 */
function projectus_exist_timetrackings_by_task($id_task) {

  $exist_timetrackings = db_query("select exists
                  (SELECT id_task
                   FROM projectus_timetrackings
                   WHERE id_task = " . $id_task . ")"
  )->fetchField();
  return $exist_timetrackings;
}

/**
 * Check if exist a especified client in database.
 *
 * Return bool.
 */
function projectus_exist_client_active($nid) {

  $exist_client = db_query("select exists
                  (SELECT nid
                   FROM projectus_clients
                   WHERE nid = " . $nid . "
                   AND active = 1)"
  )->fetchField();
  return $exist_client;
}

/**
 * Check if exist a especified project in database.
 *
 * Return bool.
 */
function projectus_exist_project_active($nid) {

  $exist_project = db_query("select exists
                  (SELECT nid
                   FROM projectus_projects
                   WHERE nid = " . $nid . "
                   AND active = 1)"
  )->fetchField();
  return $exist_project;
}

/**
 * Check if exist a especified task in database.
 *
 * Return bool.
 */
function projectus_exist_task_active($nid) {

  $exist_task = db_query("select exists
                  (SELECT nid
                   FROM projectus_tasks
                   WHERE nid = " . $nid . "
                   AND active = 1)"
  )->fetchField();
  return $exist_task;
}

/**
 * Check if exist timetrackings in database.
 *
 * Return bool.
 */
function projectus_exist_timetrackings() {

  $exist = db_query("select exists
                  (SELECT nid
                   FROM projectus_timetrackings)"
  )->fetchField();
  return $exist;
}

/**
 * Check if exist timetracking with especified task, date and uid in database.
 *
 * Return timetracking nid.
 */
function projectus_timetrackings_exist_timetracking_task_day_user($id_task, $date, $uid) {
  $query = db_select('node', 'n');
  $query->join('projectus_timetrackings', 'pt', 'n.nid = pt.nid');
  $query
  ->fields('pt', array('nid'))
  ->condition('pt.id_task', $id_task)
  ->condition('date_timetracking', $date)
  ->condition('n.uid', $uid)
  ->condition('n.status', 1);

  $nid = $query->execute()->fetchField();

  return $nid;
}

/**
 * Check if exist tasks actives assigned to especified user selected.
 *
 * Return bool.
 */
function projectus_exist_tasks_assigned_user_selected($uid) {

  $exist_tasks = db_query("select exists
                  (SELECT id_task
                   FROM projectus_tasks_users ptu, projectus_tasks pt
                   WHERE ptu.id_user = " . $uid . "
                   AND ptu.id_task = pt.nid
                   AND pt.active = 1
                   AND ptu.selected = 1)"
  )->fetchField();
  return $exist_tasks;
}

/**
 * Check if exist tasks actives assigned to especified user in database.
 *
 * Return bool.
 */
function projectus_exist_tasks_assigned_user($uid) {

  $exist_tasks = db_query("select exists
                  (SELECT id_task
                   FROM projectus_tasks_users ptu, projectus_tasks pt
                   WHERE ptu.id_user = " . $uid . "
                   AND ptu.id_task = pt.nid
                   AND pt.active = 1)"
  )->fetchField();
  return $exist_tasks;
}

/**
 * Check if exist content type deactivated in database.
 *
 * Return bool.
 */
function projectus_exist_content_deactivated() {

  $exist = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_clients pc, projectus_projects pp, projectus_tasks pt, node n
                   WHERE (pc.active = 0 AND pc.nid = n.nid)
                   OR (pp.active = 0 AND pp.nid = n.nid)
                   OR (pt.active = 0 AND pt.nid = n.nid))"
  )->fetchField();
  return $exist;
}

/**
 * Check if exist clients deactivated in database.
 *
 * Return bool.
 */
function projectus_clients_exist_content_deactivated() {

  $exist = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_clients pc, node n
                   WHERE pc.active = 0 AND pc.nid = n.nid)"
  )->fetchField();
  return $exist;
}

/**
 * Check if exist projects deactivated in database.
 *
 * Return bool.
 */
function projectus_projects_exist_content_deactivated() {

  $exist = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_projects pp, node n
                   WHERE pp.active = 0 AND pp.nid = n.nid)"
  )->fetchField();
  return $exist;
}

/**
 * Check if exist tasks deactivated in database.
 *
 * Return bool.
 */
function projectus_tasks_exist_content_deactivated() {

  $exist = db_query("select exists
                  (SELECT n.nid
                   FROM projectus_tasks pt, node n
                   WHERE pt.active = 0 AND pt.nid = n.nid)"
  )->fetchField();
  return $exist;
}


/**
 * VALIDATE FUNCTIONS.
 */

/**
 * Validates if a field is numeric.
 */
function projectus_validate_numeric($element, &$form_state) {

  if (!empty($element['#value']) && !is_numeric($element['#value'])) {
    form_error($element, t("@title is not numeric", array("@title" => $element['#title'])));
  }
}

/**
 * Validates email address format.
 */
function projectus_validate_email($element, &$form_state) {
  if (!empty($element['#value']) && !valid_email_address($element['#value'])) {
    form_error($element, t('Email is incorrect'));
  }
}

/**
 * Validates phone format.
 */
function projectus_validate_phone($element, &$form_state) {

  if (!empty($element['#value']) && !preg_match('/(^[0-9]{9}$)/', $element['#value'])) {
    form_error($element, t('Phone is incorrect'));
  }
}

/**
 * Validates cif format.
 */
function projectus_validate_cif($element, &$form_state) {

}
