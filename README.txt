PROJECTUS

ProjectUS is a project manager, built upon a Drupal platform, which allows
to control and administer a company set of tasks.
The main aim of this tool is helping project managers in their job by making
easier the organization and administration of their company.

ProjectUS can handle clients, projects, tasks and work schedules,
as well as generating detailed reports about employees working hours.

The choice of this particular platform is based on the commitment
OvertUS makes to support free software,
a foundation the company likes fitting within its planning as a way
of corresponding the community and its effort,
which everyone is benefiting.


SETUP

1. Install ProjectUS and your submodules to manage clients,
projects, tasks, timetrackings and generate reports.
(unpacking it to your Drupal sites/all/modules directory
if you're installing by hand, for example).
2. Enable ProjectUS in Admin menu > Site building > Modules.
3. Put access permissions to your preferences.
4. Now, you can access to ProjectUS main page -> projectus/main
5. Enjoy it!
