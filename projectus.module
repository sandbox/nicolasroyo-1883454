<?php

/**
 * @file
 * Module file for projectus.
 */

/**
 * Implements hook_menu().
 */
function projectus_menu() {
  $items = array();

  $items['projectus/main'] = array(
    'title' => 'ProjectUS',
    'description' => 'Project management ProjectUS',
    'page callback' => 'projectus_main',
    'access arguments' => array('projectus access main page'),
    'menu_name' => 'main-menu',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['projectus/manage-content/main'] = array(
    'title' => 'Manage content',
    'description' => 'Project management ProjectUS',
    'page callback' => 'projectus_manage_content_main',
    'access arguments' => array('projectus access manage content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements projectus_main().
 */
function projectus_main() {
  $links = module_invoke_all('projectus_main_links');
  return theme('projectus_main', $links);
}

/**
 * Implements projectus_manage_content_main().
 */
function projectus_manage_content_main() {
  return theme('projectus_manage_content_main');
}

/**
 * Implements hook_theme().
 */
function projectus_theme() {
  return array(
    'projectus_main' => array(
      'file' => 'projectus.theme.inc',
      'arguments' => array('links' => array()),
    ),
    'projectus_manage_content_main' => array(
      'file' => 'projectus.theme.inc',
      'arguments' => array(),
    ),
  );
}

/**
 * Implements hook_form_alter().
 */
function projectus_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'views_form_projectus_manage_content_page') {
    if ($form_state['step'] == 'views_bulk_operations_confirm_form') {
      drupal_add_css("ul.action-links {display:none}", 'inline');
    }
    else {
      if (empty($form['output']['#markup'])) {
        if (preg_match('/title=\S/', $form['#action'])) {
          drupal_add_css(".vbo-views-form .fieldset-wrapper {display:none}", 'inline');
          $form['output']['#markup'] = 'There aren\'t results in database with this filter.';
        }
        else {
          drupal_add_css(".vbo-views-form .fieldset-wrapper, .views-exposed-form {display:none }", 'inline');
          $form['output']['#markup'] = 'There aren\'t deactivated content in database.';
        }

        $form_state['rebuild'] = TRUE;
      }
    }
  }
}

/**
 * Implements hook_init().
 */
function projectus_init() {
  module_load_include('inc', 'projectus', 'projectus.functions');
}

/**
 * Implements hook_views_api().
 *
 * It's necessary for projectus_view.
 */
function projectus_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'projectus'),
  );
}

/**
 * Implements hook_permission().
 */
function projectus_permission() {
  return array(
    'projectus access main page' => array(
      'title' => t('Access main page'),
    ),
    'projectus access administration page' => array(
      'title' => t('Administer ProjectUS'),
    ),
    'projectus access manage content' => array(
      'title' => t('Access manage content'),
    ),
  );
}

/**
 * Implements hook_action_info().
 */
function projectus_action_info() {
  return array(
    'projectus_activate_action' => array(
      'type' => 'node',
      'label' => t('Activate'),
      'configurable' => FALSE,
      'triggers' => array('any'),
    ),
  );
}

/**
 * Implements projectus_activate_action().
 */
function projectus_activate_action($node, $context) {

  $node = node_load($node->nid);

  switch ($node->type) {
    case 'projectus_clients':
      $message = '';
      break;

    case 'projectus_projects':
      if (!projectus_exist_client_active($node->id_client)) {
        $client = node_load($node->id_client);
        $client->active = 1;
        node_save($client);
        $message = "client: '" . $client->title . "'";
      }
      break;

    case 'projectus_tasks':
      if (!projectus_exist_project_active($node->id_project)) {
        $project = node_load($node->id_project);
        $project->active = 1;
        node_save($project);
        $message = "project: '" . $project->title . "'";

        if (!projectus_exist_client_active($project->id_client)) {
          $client = node_load($project->id_client);
          $client->active = 1;
          node_save($client);
          $message .= ", client: '" . $client->title . "'";
        }
      }
      break;
  }

  $node->active = 1;
  node_save($node);
  drupal_set_message(t("':title', :message -> ACTIVATED"), array(':title' => $node->title, ':message' => $message));
}

/**
 * Implements projectus_projectus_main_links().
 */
function projectus_projectus_main_links() {

  $links = array();
  $links[] = array(
    'theme' => 'projectus_main_link',
    'title' => t('Manage content'),
    'icon' => 'projectus_manage_content',
    'path' => 'projectus/manage-content/main',
    'access_arguments' => 'projectus access manage content',
    'exist' => TRUE,
    'weight' => 5,
  );

  return $links;
}
