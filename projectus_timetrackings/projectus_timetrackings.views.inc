<?php

/**
 * @file
 * View file for projectus_timetrackings.
 */

/**
 * Implements hook_views_post_execute().
 */
function projectus_timetrackings_views_post_execute(&$view) {
  if (isset($view->name) && $view->name == 'projectus_manage_timetrackings') {
    $breadcrumb = array();
    $breadcrumb[] = l(t('ProjectUS'), 'projectus/main');
    $breadcrumb[] = l(t('Manage content'), 'projectus/manage-content/main');
    $breadcrumb[] = l(t('Manage Timetrackings'), 'projectus/manage-content/timetrackings');
    drupal_set_breadcrumb($breadcrumb);
  }
}

/**
 * Implements hook_views_data().
 *
 * Functions to expose ProjectUS Timetrackings data to views.
 */
function projectus_timetrackings_views_data() {

  // First, the entry $data['example_table']['table'] describes properties of
  // the actual table – not its content.
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['projectus_timetrackings']['table']['group'] = t('ProjectUS Timetrackings');

  // Define this as a base table – a table that can be described in itself by
  // views (and not just being brought in as a relationship). In reality this
  // is not very useful for this table, as it isn't really a distinct object of
  // its own, but it makes a good example.
  $data['projectus_timetrackings']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'nid',
    'title' => t('ProjectUS Timetrackings'),
    'help' => t('ProjectUS Timetrackings table contains example content and can be related to nodes.'),
    'weight' => -10,
  );

  // This table references the {node} table. The declaration below creates an
  // 'implicit' relationship to the node table, so that when 'node' is the base
  // table, the fields are automatically available.
  $data['projectus_timetrackings']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['projectus_timetrackings']['id_task'] = array(
    'title' => t('Id Task'),
    'help' => t('ProjectUS Timetracking Id_task'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'title' => t('id_task => node->nid'),
      'label' => t('id_task => node->nid'),
    ),
    'field' => array('click sortable' => TRUE),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'argument' => array('handler' => 'views_handler_argument_string'),
  );

  $data['projectus_timetrackings']['date_timetracking'] = array(
    'title' => t('Date timetracking'),
    'help' => t('ProjectUS Timetracking Date'),
    'field' => array(
      'handler' => 'ProjectusViewsHandlerFieldDate',
      'click sortable' => TRUE,
    ),
    'sort' => array('handler' => 'ProjectusViewsHandlerSortDate'),
    'filter' => array('handler' => 'ProjectusViewsHandlerFilterDate'),
    'argument' => array('handler' => 'views_handler_argument_string'),
  );

  $data['projectus_timetrackings']['hours'] = array(
    'title' => t('Hours'),
    'help' => t('ProjectUS Timetracking Hours'),
    'field' => array('click sortable' => TRUE),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'argument' => array('handler' => 'views_handler_argument_string'),
  );

  $data['projectus_timetrackings']['type_timetracking'] = array(
    'title' => t('Type timetracking'),
    'help' => t('ProjectUS Timetracking type'),
    'field' => array('click sortable' => TRUE),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'argument' => array('handler' => 'views_handler_argument_string'),
  );

  $data['projectus_timetrackings']['validate'] = array(
    'title' => t('Validate'),
    'help' => t('ProjectUS Timetracking validate'),
    'field' => array('click sortable' => TRUE),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
    'argument' => array('handler' => 'views_handler_argument_string'),
  );

  return $data;
}

/**
 * Implements hook_views_default_views().
 *
 * Create a view for ProjectUS Timetrackings.
 */
function projectus_timetrackings_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'projectus_manage_timetrackings';
  $view->description = '';
  $view->tag = 'projectus';
  $view->base_table = 'node';
  $view->human_name = 'projectus_manage_timetrackings';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'ProjectUS Manage Timetrackings';
  $handler->display->display_options['css_class'] = 'projectus-manage-timetrackings-view';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'ProjectUS: access manage content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« primera';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ anterior';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'siguiente ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'última »';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nothing' => 'nothing',
    'title' => 'title',
    'cif' => 'cif',
    'edit_node' => 'edit_node',
    'delete_node' => 'edit_node',
  );
  $handler->display->display_options['style_options']['default'] = 'title';
  $handler->display->display_options['style_options']['info'] = array(
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cif' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => ' ',
      'empty_column' => 0,
    ),
    'delete_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Contenido: Autor */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: ProjectUS Timetrackings: id_task => node->nid */
  $handler->display->display_options['relationships']['id_task']['id'] = 'id_task';
  $handler->display->display_options['relationships']['id_task']['table'] = 'projectus_timetrackings';
  $handler->display->display_options['relationships']['id_task']['field'] = 'id_task';
  /* Campo: Bulk operations: Contenido */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::projectus_activate_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_assign_owner_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::projectus_clients_deactivate_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::projectus_projects_deactivate_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::projectus_tasks_deactivate_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_save_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_unsticky_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_sticky_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_promote_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::projectus_timetrackings_not_validate_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 1,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::node_publish_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpromote_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_by_keyword_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::projectus_timetrackings_validate_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 1,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Campo: Usuario: Nombre */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'User';
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Campo: Contenido: Título */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'id_task';
  $handler->display->display_options['fields']['title']['label'] = 'Task';
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Campo: ProjectUS Timetrackings: Date timetracking */
  $handler->display->display_options['fields']['date_timetracking']['id'] = 'date_timetracking';
  $handler->display->display_options['fields']['date_timetracking']['table'] = 'projectus_timetrackings';
  $handler->display->display_options['fields']['date_timetracking']['field'] = 'date_timetracking';
  $handler->display->display_options['fields']['date_timetracking']['label'] = 'Date';
  /* Campo: ProjectUS Timetrackings: Hours */
  $handler->display->display_options['fields']['hours']['id'] = 'hours';
  $handler->display->display_options['fields']['hours']['table'] = 'projectus_timetrackings';
  $handler->display->display_options['fields']['hours']['field'] = 'hours';
  /* Campo: ProjectUS Timetrackings: Validate */
  $handler->display->display_options['fields']['validate']['id'] = 'validate';
  $handler->display->display_options['fields']['validate']['table'] = 'projectus_timetrackings';
  $handler->display->display_options['fields']['validate']['field'] = 'validate';
  /* Sort criterion: Contenido: Título */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Contenido: Publicado */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Contenido: Tipo */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'projectus_timetrackings' => 'projectus_timetrackings',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Usuario: Nombre */
  $handler->display->display_options['filters']['uid_1']['id'] = 'uid_1';
  $handler->display->display_options['filters']['uid_1']['table'] = 'users';
  $handler->display->display_options['filters']['uid_1']['field'] = 'uid';
  $handler->display->display_options['filters']['uid_1']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid_1']['value'] = '';
  $handler->display->display_options['filters']['uid_1']['group'] = 1;
  $handler->display->display_options['filters']['uid_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid_1']['expose']['operator_id'] = 'uid_1_op';
  $handler->display->display_options['filters']['uid_1']['expose']['label'] = 'User';
  $handler->display->display_options['filters']['uid_1']['expose']['operator'] = 'uid_1_op';
  $handler->display->display_options['filters']['uid_1']['expose']['identifier'] = 'uid_1';
  $handler->display->display_options['filters']['uid_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: Contenido: Título */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['relationship'] = 'id_task';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Task';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: ProjectUS Timetrackings: Validate */
  $handler->display->display_options['filters']['validate']['id'] = 'validate';
  $handler->display->display_options['filters']['validate']['table'] = 'projectus_timetrackings';
  $handler->display->display_options['filters']['validate']['field'] = 'validate';
  $handler->display->display_options['filters']['validate']['group'] = 1;
  $handler->display->display_options['filters']['validate']['exposed'] = TRUE;
  $handler->display->display_options['filters']['validate']['expose']['operator_id'] = 'validate_op';
  $handler->display->display_options['filters']['validate']['expose']['label'] = 'Validate';
  $handler->display->display_options['filters']['validate']['expose']['operator'] = 'validate_op';
  $handler->display->display_options['filters']['validate']['expose']['identifier'] = 'validate';
  $handler->display->display_options['filters']['validate']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['validate']['group_info']['label'] = 'Validate';
  $handler->display->display_options['filters']['validate']['group_info']['identifier'] = 'validate';
  $handler->display->display_options['filters']['validate']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'Not validate',
      'operator' => '!=',
      'value' => '1',
    ),
    2 => array(
      'title' => 'Validate',
      'operator' => '=',
      'value' => '1',
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
  );
  /* Filter criterion: ProjectUS Timetrackings: Date timetracking */
  $handler->display->display_options['filters']['date_timetracking']['id'] = 'date_timetracking';
  $handler->display->display_options['filters']['date_timetracking']['table'] = 'projectus_timetrackings';
  $handler->display->display_options['filters']['date_timetracking']['field'] = 'date_timetracking';
  $handler->display->display_options['filters']['date_timetracking']['operator'] = 'between';
  $handler->display->display_options['filters']['date_timetracking']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_timetracking']['expose']['operator_id'] = 'date_timetracking_op';
  $handler->display->display_options['filters']['date_timetracking']['expose']['operator'] = 'date_timetracking_op';
  $handler->display->display_options['filters']['date_timetracking']['expose']['identifier'] = 'date_timetracking';
  $handler->display->display_options['filters']['date_timetracking']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['date_timetracking']['form_type'] = 'date_popup';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'projectus/manage-content/timetrackings';
  $translatables['projectus_timetrackings_validate'] = array(
    t('Master'),
    t('ProjectUS Manage Timetrackings'),
    t('more'),
    t('Apply'),
    t('Reiniciar'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« primera'),
    t('‹ anterior'),
    t('siguiente ›'),
    t('última »'),
    t('author'),
    t('id_task => node->nid'),
    t('Contenido'),
    t('User'),
    t('Task'),
    t('Date'),
    t('Hours'),
    t('Validate'),
    t('Page'),
  );

  $views[$view->name] = $view;

  return $views;
}
