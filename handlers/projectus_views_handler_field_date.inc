<?php

/**
 * @file
 * Handler file for projectus field date.
 */

/**
 * A handler to display dates that are DATETIME instead of unix timestamp.
 *
 * @ingroup views_field_handlers
 */
class ProjectusViewsHandlerFieldDate extends Views_handler_field_date {
  /**
   * Implements render for a field.
   *
   * @values: field to render.
   */
  public function render($values) {
    $value = $values->{$this->field_alias};
    return date('Y-m-d', strtotime($value));
  }

  /**
   * Implements getvalue for a constructor.
   *
   * @values: value to assign.
   * @field: field to assign.
   */
  protected function getvalue($values, $field = NULL) {
    $value = parent::getvalue($values, $field);
    if (!empty($value)) {
      // Convert database datetime value to timestamp,
      // so that the date handler understands it.
      $datetime = new DateTime($value);
      $value = $datetime->getTimestamp();
    }
    return $value;
  }
}

/**
 * A handler to filter dates that are DATETIME instead of unix timestamp.
 *
 * @ingroup views_filter_handlers
 */
class ProjectusViewsHandlerFilterDate extends Date_views_filter_handler_simple {
  /**
   * Implements filter for a data field.
   *
   * @field: field to filter.
   */
  protected function opbetween($field) {
    parent::opbetween("UNIX_TIMESTAMP(" . $field . ")");
  }

  /**
   * Implements filter simple for a data field.
   *
   * @field: field to filter.
   */
  protected function opsimple($field) {
    parent::opsimple("UNIX_TIMESTAMP(" . $field . ")");
  }
}

/**
 * A handler to sort dates that are DATETIME instead of unix timestamp.
 *
 * @ingroup views_sort_handlers
 */
class ProjectusViewsHandlerSortDate extends Views_handler_sort_date {

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    $this->ensure_my_table();

    switch ($this->options['granularity']) {
      case 'second':
      default:
        $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order']);
        return;

      case 'minute':
        $formula = "DATE_FORMAT({$this->table_alias}.{$this->real_field}, '%Y%m%d%H%i')";
        break;

      case 'hour':
        $formula = "DATE_FORMAT({$this->table_alias}.{$this->real_field}, '%Y%m%d%H')";
        break;

      case 'day':
        $formula = "DATE_FORMAT({$this->table_alias}.{$this->real_field}, '%Y%m%d')";
        break;

      case 'month':
        $formula = "DATE_FORMAT({$this->table_alias}.{$this->real_field}, '%Y%m')";
        break;

      case 'year':
        $formula = "DATE_FORMAT({$this->table_alias}.{$this->real_field}, '%Y')";
        break;

    }

    // Add the field.
    $this->query->add_orderby(NULL, $formula, $this->options['order'], $this->table_alias . '_' . $this->field . '_' . $this->options['granularity']);
  }
}
