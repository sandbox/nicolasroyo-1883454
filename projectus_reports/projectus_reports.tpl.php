<?php

/**
 * @file
 * TPL file for projectus_reports.
 */

  $start_date = $_GET['start_date'];
  $end_date = $_GET['end_date'];
  $client = 0;
  $project = 0;
  $task = 0;

  if (!empty($_GET['user'])) :
    $users = array($_GET['user'] => $_GET['user']);
    $users_text = user_load($_GET['user'])->name;
  else :
    $users = projectus_get_all_users();
    $users_text = 'All users';
  endif;

  if (!empty($_GET['client'])) :
    $client = $_GET['client'];
    $client_text = node_load($_GET['client'])->title;
  endif;

  if (!empty($_GET['project'])) :
    $project = $_GET['project'];
    $project_text = node_load($_GET['project'])->title;
  endif;

  if(!empty($_GET['task'])) :
    $task = $_GET['task'];
    $task_text = node_load($_GET['task'])->title;
  endif;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang=EN" xml:lang="EN">
  <head>
    <title>Report timetrackings - <?php print $users_text; ?> - <?php print $start_date . ' to ' . $end_date ?></title>
    <link rel="stylesheet" type="text/css" href="<?php print drupal_get_path('module', 'projectus_reports') . '/projectus_reports.css' ?>" media="screen" />
  </head>
  <body>
    <hr class="report-hr" />
    <h1>Report timetrackings</h1>
    <h2>Filter by:</h2>
    <h3>Date: <?php print $start_date . ' to ' . $end_date ?></h3>
    <h3>Users: <?php print $users_text; ?></h3>
    <?php if(!empty($_GET['client'])) :?>
      <h3>Client: <?php print $client_text; ?></h3>
    <?php endif; ?>
    <?php if(!empty($_GET['project'])) :?>
      <h3>Project: <?php print $project_text; ?></h3>
    <?php endif; ?>
    <?php if(!empty($_GET['task'])) :?>
      <h3>Task: <?php print $task_text; ?></h3>
    <?php endif; ?>

    <hr class="report-hr" />

    <form>
      <input type="button" value="Print this page" onClick="window.print()">
      <?php print l(t('Back'), 'projectus/manage-content/generate-reports'); ?>
    </form>

    <hr class="report-hr" />

    <table class="report-table" border="1">
    <tr>
      <th class="report-th">User</th>
      <th class="report-th">Validate</th>
      <th class="report-th">Not validate</th>
      <th class="report-th">Total hours</th>
    </tr>

    <?php
      foreach($users as $uid => $name) :
        $user = user_load($uid);
        $validate = projectus_reports_get_hours($uid, 1, $client, $project, $task, $start_date, $end_date);
        $not_validate = projectus_reports_get_hours($uid, 0, $client, $project, $task, $start_date, $end_date);
        $total = $validate + $not_validate;
    ?>
    <tr>
      <td><?php print $user->name; ?></td>
      <td><?php print (int) $validate; ?></td>
      <td><?php print (int) $not_validate; ?></td>
      <td><?php print $total; ?></td>
    </tr>

    <?php endforeach; ?>

    </table>

    <hr class="report-hr" />

    <form>
      <input type="button" value="Print this page" onClick="window.print()">
      <?php print l(t('Back'), 'projectus/manage-content/generate-reports'); ?>
    </form>
  </body>
</html>
