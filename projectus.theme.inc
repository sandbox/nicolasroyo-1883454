<?php

/**
 * @file
 * Theme file for projectus.
 */

/**
 * Implements theme_projectus_main($links).
 */
function theme_projectus_main($links) {

  $breadcrumb = array();
  $breadcrumb[] = l(t('ProjectUS'), 'projectus/main');
  drupal_set_breadcrumb($breadcrumb);

  $empty_bool = TRUE;

  $content = '<div id="projectus_main">';

  if (!empty($links)) {

    foreach ($links as $key => $row) {
      $weight[$key] = $row['weight'];
    }

    array_multisort($weight, SORT_ASC, $links);

    foreach ($links as $link_id => $link_array) {

      if (user_access($link_array['access_arguments'])) {

        $empty_bool = FALSE;

        $content .= '<div class="projectus_main_link" id=link_' . $link_id . '>';

        $variables = array(
          'path' => drupal_get_path('module', 'projectus') . '/styles/icons/' . $link_array['icon'] . '.png',
          'alt' => $link_array['title'],
          'title' => $link_array['title'],
        );

        $img = theme('image', $variables);

        $image = '<div>' . $img . '</div><div>' . $link_array['title'] . '</div>';

        if ($link_array['exist']) {
          $content .= l($image, $link_array['path'], array('html' => TRUE));
        }
        else {
          $content .= $image;
        }

        $content .= '</div>';
      }
    }

    if ($empty_bool) {
      $content .= t('No options available. Request permission to access them.');
    }
  }
  else {
    $content .= t('No options available.');
  }

  $content .= '</div>';

  return $content;
}

/**
 * Implements theme_projectus_manage_content_main().
 */
function theme_projectus_manage_content_main() {

  $breadcrumb = array();
  $breadcrumb[] = l(t('ProjectUS'), 'projectus/main');
  $breadcrumb[] = l(t('Manage content'), 'projectus/manage-content/main');
  drupal_set_breadcrumb($breadcrumb);

  $content = '<div id="projectus_main_link">';

  $content .= '<div class="projectus_main_link">';

  $variables = array(
    'path' => drupal_get_path('module', 'projectus') . '/styles/icons/projectus_manage_content.png',
    'alt' => 'Content deactivated',
    'title' => 'Content deactivated',
  );

  $img = theme('image', $variables);
  $image = '<div>' . $img . '</div><div>Content deactivated</div>';

  if ((module_exists('projectus_clients') && projectus_clients_exist_content_deactivated()) ||
    (module_exists('projectus_projects') && projectus_projects_exist_content_deactivated()) ||
    (module_exists('projectus_tasks') && projectus_tasks_exist_content_deactivated())) {
    $content .= l($image, 'projectus/manage-content/deactivated', array('html' => TRUE));
  }
  else {
    $content .= $image;
  }

  $content .= '</div>';

  if (module_exists('projectus_timetrackings')) {
    $content .= '<div id="projectus_main_link">';

    $content .= '<div class="projectus_main_link">';

    $variables = array(
      'path' => drupal_get_path('module', 'projectus') . '/styles/icons/projectus_manage_content.png',
      'alt' => 'Manage timetrackings',
      'title' => 'Manage timetrackings',
    );

    $img = theme('image', $variables);
    $image = '<div>' . $img . '</div><div>Manage timetrackings</div>';

    if (projectus_exist_timetrackings()) {
      $content .= l($image, 'projectus/manage-content/timetrackings', array('html' => TRUE));
    }
    else {
      $content .= $image;
    }

    $content .= '</div>';

    $content .= '</div>';
  }

  if (module_exists('projectus_reports') && user_access('ProjectUS reports: access')) {
    $content .= '<div id="projectus_main_link">';

    $content .= '<div class="projectus_main_link">';

    $variables = array(
      'path' => drupal_get_path('module', 'projectus') . '/styles/icons/projectus_manage_content.png',
      'alt' => 'Generate reports',
      'title' => 'Generate reports',
    );

    $img = theme('image', $variables);
    $image = '<div>' . $img . '</div><div>Generate reports</div>';

    $content .= l($image, 'projectus/manage-content/generate-reports', array('html' => TRUE));

    $content .= '</div>';

    $content .= '</div>';
  }

  return $content;
}

/**
 * Implements theme_projectus_clients_view_item($variables).
 */
function theme_projectus_clients_view_item($variables) {
  return '<div class="label"><span class="label">' . $variables['label'] . ':&nbsp;</span></div><div class="value">' . $variables['value'] . '&nbsp;</div>';
}

/**
 * Implements theme_projectus_projects_view_item($variables).
 */
function theme_projectus_projects_view_item($variables) {
  return '<div class="label"><span class="label">' . $variables['label'] . ':&nbsp;</span></div><div class="value">' . $variables['value'] . '&nbsp;</div>';
}

/**
 * Implements theme_projectus_tasks_view_item($variables).
 */
function theme_projectus_tasks_view_item($variables) {
  return '<div class="label"><span class="label">' . $variables['label'] . ':&nbsp;</span></div><div class="value">' . $variables['value'] . '&nbsp;</div>';
}

/**
 * Implements theme_projectus_timetrackings_view_item($variables).
 */
function theme_projectus_timetrackings_view_item($variables) {
  return '<div class="label"><span class="label">' . $variables['label'] . ':&nbsp;</span></div><div class="value">' . $variables['value'] . '&nbsp;</div>';
}
